 # LE VY PRIVACY POLICY #
 
 We are committed to protecting the privacy of persons, especially children, who use Kiddbee.

Please read this Privacy Policy carefully.

# 1. General information #

1.1 Kiddbee is primarily targeted at children and provides educational games for children.

1.2 This Privacy Policy contains our policies and procedures governing the collection, use, and disclosure of personal data and the parental consent practices. For the purposes of this Privacy Policy:

The term “personal data” refers to any information relating to a natural person who can be identified, directly or indirectly, by using such information; and
The term “processing” refers to collection, storage, erasure, use, and disclosure of personal data.
1.3 By using Kiddbee and/or providing your verifiable consent, you acknowledge that you have read this Privacy Policy and you agree to be bound by it. If you do not agree with one or more provisions of this Privacy Policy, please do not use Kiddbee.



# 2. Kinds of personal data collected by us # 

2.1 Taking into account that Kiddbee is primarily targeted at children, we respect strictest data protection principles.

2.2 When you use Kiddbee, we do not collect any personal data from you. Our analytics functionality may have access to your IP address. However, we neither store nor use or access your IP address for any purposes. Your IP address is not used, in any manner, including in combination with other information, to identify you.

2.3 The personal data listed below may be provided by you voluntarily:

When you send us feedback or contact us via email or the contact form available on Kiddbee, we collect your (i) name, (ii) email address, and (iii) any other personal data you decide to provide us. We delete such personal data from our databases as soon as we reply to your message.
When you subscribe for a newsletter, we collect your email address. Please note that, if you are under the age of 16, the newsletter is delivered only after obtaining verifiable parental consent.
2.4 We shall not, in any manner, make your personal data public.

2.5 Sensitive data. We do not collect, under any circumstances, any sensitive data from you, such as your health information, opinion about your religious and political beliefs, ethnic origins, membership of a professional or trade association, or information about your sexual orientation.


# 3. Kinds of non-personal data collected by us #

3.1 When you use Kiddbee, we may collect your non-personal data. The non-personal data includes analytics data, such as your activity on Kiddbee, the browser types used by you, operating systems, and the URL addresses of websites clicked to and from Kiddbee. The non-personal data collected by us does not allow us to identify you in any manner.

3.2 Aggregated data. We do not aim to aggregate your personal data and non-personal data. In case your non-personal data is aggregated with certain elements of your personal data in the way that allows us to identify you, we will treat such aggregated data as personal data.

3.3 Purposes of non-personal data. The non-personal data is used to analyze what kind of users visit Kiddbee, how they find it, how long they stay, from which other websites they come to Kiddbee, what pages they look at, what functionalities of Kiddbee they use, and to which other websites they go from Kiddbee. We use the non-personal data to improve our business activities and provide you with the best possible services.

# 4. The purpose of collection of your personal data #

4.1 We use your personal data only for the purposes for which such personal data is provided:

Your email address, name, and other personal data (if any) submitted through the contact form are used to reply to your requests on a one-time basis. After we reply to your request, we immediately delete such personal data.
Your email address provided for subscribing to our newsletter is used to deliver our newsletter and provide you with advertisements of products and services, which may be of interest to you.
4.2 In certain exceptional cases, the personal data collected by us may also be used for audit and security purposes.

4.3 Legal grounds for data processing. Depending on the circumstances in which you provide your personal data to us, the collection and processing of your personal data by us is carried out on the basis of one of the following legal grounds:

Pursuing our legitimate business interests (e.g., analyzing the usage of Kiddbee);
If you have provided us with your explicit consent; or
If we are required by law to do so.

# 5. Protection of your personal data #

5.1 We will put reasonable efforts to maintain the security of and to prevent misuse, loss, unauthorized access, and modification of your personal data. We use organizational and technical security measures to protect all types of personal data, such as limited access to your personal data by our staff, secured networks, and encryption.

5.2 Security breaches. Please note that, due to the inherent risks of using the Internet, we cannot be liable for any unlawful destruction, loss, use, copying, modification, leakage, and falsification of your personal data caused by circumstances beyond our reasonable control. In case a personal data breach occurs, we will inform the relevant data protection authority without undue delay and immediately take reasonable measures to mitigate the breach, as stipulated in the applicable law.

# 6. Disclosure of personal data to third parties #

6.1 In certain situations, we may disclose your personal data to third parties. Such a disclosure is limited to the situations when the personal data is required for the following purposes:

Ensuring the operation of Kiddbee;
Pursuing our legitimate interests;
Carrying out our contractual obligations;
Law enforcement purposes; or
If you provide your prior explicit consent.
6.2 Your personal data may be disclosed to third parties that provide professional, technical, and other types of support to us, including: Google, Firebase

6.3 The third parties listed in this Section 6 will access your personal data as a part of their partnership with us and only if they agree to ensure an adequate level of protection of personal data that is consistent with this Privacy Policy.

6.5 We may share aggregated or anonymized information and non-personal data with third parties for the purposes of developing services that may be of interest to you.

6.6 With the exception of the cases in this Section 6, we do not disclose, without your prior verifiable consent, or sell your personal data to third parties and we do not intend to do so in the future.

# 8. Exercising your rights #

8.1 We take reasonable steps to ensure that the personal data collected and processed by us is accurate, complete, and up-to-date.

8.2 We provide you with the possibility to exercise your rights regarding your personal data. You can request us to:

Access your personal data that we retain;
Move your personal data to another entity;
Delete your personal data from our systems;
Object and restrict processing of your personal data;
Withdraw your consent;
Process your complaint.
8.3 If you would like to exercise your rights listed above, please contact us by email at daturit2626@gmail.com. We may ask you to provide us with an identifying piece of data, so that we would be able to identify you in our system. We will answer your request within a reasonable timeframe but no later than 1 week.

8.4 Launching a complaint. If you would like to launch a complaint about the way in which your personal data is handled, we kindly ask to contact us first. After you contact us, we will investigate your complaint and provide you with our response as soon as possible. If you are not satisfied with the outcome of your complaint, you have the right to lodge a complaint with your local data protection authority.

# 9. Retention period #

9.1 Your personal data will be kept for as long as is necessary to provide you with the requested service. For instance, if your personal data is collected to deliver the newsletter, your personal data will be kept until you unsubscribe from the newsletter service; if you contact us by using the contact form, we will retain your personal data only until we reply to your enquiry.

9.2 When your personal data is no longer necessary to deliver the requested service or you cease using Kiddbee, we will immediately delete your personal data, unless we are required by the applicable law to retain such personal data for a certain period of time.

# 10. Consent # 

10.1 We will seek explicit prior consent from you (if you are above the age of 16) or you parent or guardian (if you are under the age of 16) in the following cases:

Before using your personal data for newsletter delivery purposes;
Before responding to you more than on a one-time basis;
Before disclosing your personal data to third parties that are not mentioned in this Privacy Policy;
Before using your personal data for the purposes that are not mentioned in this Privacy Policy; and
Before collecting other kinds of your personal data that are not mentioned in this Privacy Policy.
10.2 We provide the parents or guardians of children under the age of 16 with an option to agree to the collection and use of the child’s personal data without consenting to the disclosure of the personal data to third parties.

10.3 If a person under the age of 16 has provided us with personal data without obtaining parental consent in advance, the parent or the guardian may contact us by email at info@bimiboo.com and request us to destroy or de-identify the personal data.

# 11. Links #

11.1 Kiddbee may contain links to third-party websites and services (e.g., Facebook, YouTube, and Instagram). We are not responsible in any manner for the privacy practices of those websites and services. Please review carefully the applicable privacy policies before visiting the third-party websites and services.

# 12. Cookies #

12.1 Kiddbee uses cookies (i.e., small computer files consisting of letters and numbers). When you visit a website, the website may send a cookie to your browser. Subsequently, the browser may store the cookie on your computer system. The main purpose of cookies is to allow a website to recognize user’s device.

12.2 There are two types of cookies, namely, persistent cookies and session cookies. Persistent cookies remain valid until their expiration date, unless deleted by the user before that date. Session cookies will be stored on a web browser and will remain valid until the moment when the browser is closed.

12.3 Cookies do not typically contain personal data. However, personal data stored by us may be linked to the information stored in and obtained from cookies. Web servers can use cookies to: (1) identify and track users while they navigate through different pages on a website; and (2) identify users returning to a website.

12.4 We use both session and persistent cookies. We use session cookies in order to verify your details until you are navigating from page to page on Kiddbee. The session cookies enable you to avoid re-entering your details each time you enter a new page on Kiddbee. We use persistent cookies in order to recognize you as a unique user when you return to Kiddbee. We will not use cookies for purposes which are not mentioned in this Privacy Policy.

12.5 By using Kiddbee, you agree to our use of cookies as described in this Privacy Policy. If you do not agree to our use of cookies, you need to either (1) discontinue your use of Kiddbee or (2) set your browser to refuse cookies. Please be aware that some parts of Kiddbee may not function properly without cookies. If you would like to set your browser to refuse cookies, please check your browser’s help information or https://cookies.insites.com/disable-cookies/.

# 13. Amendment of this Privacy Policy #

13.1 We reserve the right to amend this Privacy Policy from time to time by posting an amended version on Kiddbee and seeking your consent (if necessary). Such amendments may be necessary to reflect legislative changes or our new business practices. Please periodically review the Privacy Policy to stay up-to-date with the current version of the Privacy Policy.

13.2 If we retain your email address, we may, but are not under an obligation to, send you a notification about the amendments of the Privacy Policy.

# 14. Contact details #

14.1 If you have any questions or concerns about this Privacy Policy, please contact us by email at daturit2626@gmail.com




