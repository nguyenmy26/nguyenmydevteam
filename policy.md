# Privacy Policy #
Nguyen My built the iOS app as a Free app. This SERVICE is provided by Nguyen My at no cost and is intended for use as is.

This page is used to inform visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.

If you choose to use my Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Privacy Policy.

The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at iOS unless otherwise defined in this Privacy Policy.

### Information Collection and Use ###

For a better experience, while using our Service, I may require you to provide us with certain personally identifiable information. The information that I request will be retained on your device and is not collected by me in any way.

The app does use third party services that may collect information used to identify you.

I want to inform you that whenever you use my Service, in a case of an error in the app I collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing my Service, the time and date of your use of the Service, and other statistics.

### Cookies ###

Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.

This Service does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.

### Kinds of personal data collected by us#

 Taking into account that NanaBoo is primarily targeted at children, we respect strictest data protection principles.

 When you use NanaBoo, we do not collect any personal data from you. Our analytics functionality may have access to your IP address. However, we neither store nor use or access your IP address for any purposes. Your IP address is not used, in any manner, including in combination with other information, to identify you.

 The personal data listed below may be provided by you voluntarily:

When you send us feedback or contact us via email or the contact form available on NanaBoo, we collect your (i) name, (ii) email address, and (iii) any other personal data you decide to provide us. We delete such personal data from our databases as soon as we reply to your message. When you subscribe for a newsletter, we collect your email address. Please note that, if you are under the age of 16, the newsletter is delivered only after obtaining verifiable parental consent. 2.4 We shall not, in any manner, make your personal data public.

 Sensitive data. We do not collect, under any circumstances, any sensitive data from you, such as your health information, opinion about your religious and political beliefs, ethnic origins, membership of a professional or trade association, or information about your sexual orientation.
### Kinds of non-personal data collected by us

 When you use NanaBoo, we may collect your non-personal data. The non-personal data includes analytics data, such as your activity on NanaBoo, the browser types used by you, operating systems, and the URL addresses of websites clicked to and from NanaBoo. The non-personal data collected by us does not allow us to identify you in any manner.

 Aggregated data. We do not aim to aggregate your personal data and non-personal data. In case your non-personal data is aggregated with certain elements of your personal data in the way that allows us to identify you, we will treat such aggregated data as personal data.

 Purposes of non-personal data. The non-personal data is used to analyze what kind of users visit NanaBoo, how they find it, how long they stay, from which other websites they come to NanaBoo, what pages they look at, what functionalities of NanaBoo they use, and to which other websites they go from NanaBoo. We use the non-personal data to improve our business activities and provide you with the best possible services.
### The purpose of collection of your personal data

 We use your personal data only for the purposes for which such personal data is provided:

Your email address, name, and other personal data (if any) submitted through the contact form are used to reply to your requests on a one-time basis. After we reply to your request, we immediately delete such personal data. Your email address provided for subscribing to our newsletter is used to deliver our newsletter and provide you with advertisements of products and services, which may be of interest to you. 4.2 In certain exceptional cases, the personal data collected by us may also be used for audit and security purposes.

 Legal grounds for data processing. Depending on the circumstances in which you provide your personal data to us, the collection and processing of your personal data by us is carried out on the basis of one of the following legal grounds:

Pursuing our legitimate business interests (e.g., analyzing the usage of NanaBoo); If you have provided us with your explicit consent; or If we are required by law to do so.
### Protection of your personal data

 We will put reasonable efforts to maintain the security of and to prevent misuse, loss, unauthorized access, and modification of your personal data. We use organizational and technical security measures to protect all types of personal data, such as limited access to your personal data by our staff, secured networks, and encryption.

Security breaches. Please note that, due to the inherent risks of using the Internet, we cannot be liable for any unlawful destruction, loss, use, copying, modification, leakage, and falsification of your personal data caused by circumstances beyond our reasonable control. In case a personal data breach occurs, we will inform the relevant data protection authority without undue delay and immediately take reasonable measures to mitigate the breach, as stipulated in the applicable law.
### Disclosure of personal data to third parties

In certain situations, we may disclose your personal data to third parties. Such a disclosure is limited to the situations when the personal data is required for the following purposes:

Ensuring the operation of NanaBoo; Pursuing our legitimate interests; Carrying out our contractual obligations; Law enforcement purposes; or If you provide your prior explicit consent. 6.2 Your personal data may be disclosed to third parties that provide professional, technical, and other types of support to us, including: Google, Firebase

The third parties listed in this Section 6 will access your personal data as a part of their partnership with us and only if they agree to ensure an adequate level of protection of personal data that is consistent with this Privacy Policy.

We may share aggregated or anonymized information and non-personal data with third parties for the purposes of developing services that may be of interest to you.

With the exception of the cases in this Section 6, we do not disclose, without your prior verifiable consent, or sell your personal data to third parties and we do not intend to do so in the future.


### Security ###

I value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and I cannot guarantee its absolute security.

Links to Other Sites

This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by me. Therefore, I strongly advise you to review the Privacy Policy of these websites. I have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.

### Children’s Privacy ###

These Services do not address anyone under the age of 13. I do not knowingly collect personally identifiable information from children under 13. In the case I discover that a child under 13 has provided me with personal information, I immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact me so that I will be able to do necessary actions.

Changes to This Privacy Policy

I may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. I will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.

### Contact Us ###

If you have any questions or suggestions about my Privacy Policy, do not hesitate to contact me.